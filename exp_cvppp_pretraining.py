from network import *
from loader import CVPPP17Loader
import numpy as np
import argparse


parser = argparse.ArgumentParser()
parser.add_argument('--training-path',dest='training_path',action='store',type=str,required=True)
parser.add_argument('--testing-path',dest='testing_path',action='store',type=str,required=True)
parser.add_argument('--patience',dest='patience',action='store',type=int,default=10)
parser.add_argument('--split','-s',dest='split',action='store',type=int,default=0)
parser.add_argument('--test',dest='test',action='store_true')
parser.add_argument('--output','-o',dest='output',action='store',type=str,default="model.h5")
parser.add_argument('--model','-m',dest='model',action='store',type=str,default=None)
parser.add_argument('--multi',dest='multi',action='store_true')

args = parser.parse_args()

print(args)

model = GDAN()
model.MiniBatchSize=32

if (args.multi):
    model.EndLayer = FINAL_LAYER_MULTI
else:
    model.EndLayer = FINAL_LAYER_CLASSIC


model.build_source_network()

model.source_model.summary()

loader = CVPPP17Loader(args.training_path,args.testing_path,a3=False,autocontrast=True,normalise=True)

if (args.test):
    X_train,Y_train, X_test,Y_test = loader.get_data("training",args.split)
    X_test, Y_test = loader.get_data("testing", args.split)
    model.source_model.load_weights(args.model)

    Y_hat_train = model.source_model.predict(X_train)
    Y_hat_test = model.source_model.predict(X_test)
    #Y_hat = np.round(Y_hat)

    test_metric = compute_metrics(Y_test,Y_hat_test[:,0])
    print_metrics(test_metric)

    with open(args.output,'w') as h:

        for i in range(len(Y_hat_train)):
            h.write("{},{}\n".format(Y_train[i],Y_hat_train[i]))

        h.write('\n')

        for i in range(len(Y_hat_test)):
            h.write("{},{}\n".format(Y_test[i],Y_hat_test[i]))

    print ("[DONE]")

else:
    X_train,Y_train, X_test,Y_test = loader.get_data("training",args.split)

    model.pretrain_network(X_train,Y_train,X_test,Y_test,filename=args.output)

    Y_train_hat = model.source_model.predict(X_train)
    Y_test_hat  = model.source_model.predict(X_test)

    train_metric = compute_metrics(Y_train,Y_train_hat[:,0])
    test_metric = compute_metrics(Y_test,Y_test_hat[:,0])

    print(">>> TRAINING ERROR")
    print_metrics(train_metric)

    print("")
    print(">>> INTERNAL TESTING ERROR")
    print_metrics(test_metric)


