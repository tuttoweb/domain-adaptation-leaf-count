#!/bin/bash

set -e

export CUDA_VISIBLE_DEVICES=2

TRAINING_PATH="../RDS/CVPPP2017/CVPPP2017_LCC_training/TrainingSplits"
TESTING_PATH="../RDS/CVPPP_2017/CVPPP2017_testing/testing"

python exp_cvppp_pretraining.py --training-path $TRAINING_PATH --testing-path $TESTING_PATH --split 0 -m ../RDS/cvpppda/trained_models/pretrained_cvppp-fixed/cvppp_pretrained_norm_split_0.h5 --test -o results/pretraining_cvppp/singlelayer/continuous_counting/cvppp_cont_single_0.csv
python exp_cvppp_pretraining.py --training-path $TRAINING_PATH --testing-path $TESTING_PATH --split 1 -m ../RDS/cvpppda/trained_models/pretrained_cvppp-fixed/cvppp_pretrained_norm_split_1.h5 --test -o results/pretraining_cvppp/singlelayer/continuous_counting/cvppp_cont_single_1.csv
python exp_cvppp_pretraining.py --training-path $TRAINING_PATH --testing-path $TESTING_PATH --split 2 -m ../RDS/cvpppda/trained_models/pretrained_cvppp-fixed/cvppp_pretrained_norm_split_2.h5 --test -o results/pretraining_cvppp/singlelayer/continuous_counting/cvppp_cont_single_2.csv
python exp_cvppp_pretraining.py --training-path $TRAINING_PATH --testing-path $TESTING_PATH --split 3 -m ../RDS/cvpppda/trained_models/pretrained_cvppp-fixed/cvppp_pretrained_norm_split_3.h5 --test -o results/pretraining_cvppp/singlelayer/continuous_counting/cvppp_cont_single_3.csv

python exp_cvppp_pretraining.py --training-path $TRAINING_PATH --testing-path $TESTING_PATH --split 0 -m trained_models/pretrained_cvppp/cvppp_pretrained_split_multi_0.h5 --test -o results/pretraining_cvppp/sumlayer/continuous_counting/cvppp_cont_multi_0.csv --multi
python exp_cvppp_pretraining.py --training-path $TRAINING_PATH --testing-path $TESTING_PATH --split 1 -m trained_models/pretrained_cvppp/cvppp_pretrained_split_multi_1.h5 --test -o results/pretraining_cvppp/sumlayer/continuous_counting/cvppp_cont_multi_1.csv --multi
python exp_cvppp_pretraining.py --training-path $TRAINING_PATH --testing-path $TESTING_PATH --split 2 -m trained_models/pretrained_cvppp/cvppp_pretrained_split_multi_2.h5 --test -o results/pretraining_cvppp/sumlayer/continuous_counting/cvppp_cont_multi_2.csv --multi
python exp_cvppp_pretraining.py --training-path $TRAINING_PATH --testing-path $TESTING_PATH --split 3 -m trained_models/pretrained_cvppp/cvppp_pretrained_split_multi_3.h5 --test -o results/pretraining_cvppp/sumlayer/continuous_counting/cvppp_cont_multi_3.csv --multi
