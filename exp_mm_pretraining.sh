#!/bin/bash

export CUDA_VISIBLE_DEVICES=2

TRAINING_PATH="../RDS/mm"

python exp_mm_pretraining.py --dataset-path $TRAINING_PATH --split 0 -o trained_models/pretrained_mm/mm_pretrained_split_multi_0.h5 --multi
python exp_mm_pretraining.py --dataset-path $TRAINING_PATH --split 1 -o trained_models/pretrained_mm/mm_pretrained_split_multi_1.h5 --multi
python exp_mm_pretraining.py --dataset-path $TRAINING_PATH --split 2 -o trained_models/pretrained_mm/mm_pretrained_split_multi_2.h5 --multi
python exp_mm_pretraining.py --dataset-path $TRAINING_PATH --split 3 -o trained_models/pretrained_mm/mm_pretrained_split_multi_3.h5 --multi
