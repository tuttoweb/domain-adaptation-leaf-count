#!/bin/bash

export CUDA_VISIBLE_DEVICES=3

SOURCE_PATH="../RDS/CVPPP2017/CVPPP2017_LCC_training/TrainingSplits"
TARGET_PATH="../RDS/14-Frontiers/komatsuna.mat"

#python exp_intraspecies_komatsuna.py --source-path $SOURCE_PATH --target-path $TARGET_PATH --split 0 -m trained_models/pretrained_cvppp/cvppp_pretrained_split_multi_0.h5 -o trained_models/inter_species/komatsuna/split0 --generator-loops 2 --loss mse --multi
#python exp_intraspecies_komatsuna.py --source-path $SOURCE_PATH --target-path $TARGET_PATH --split 1 -m trained_models/pretrained_cvppp/cvppp_pretrained_split_multi_1.h5 -o trained_models/inter_species/komatsuna/split1 --generator-loops 2 --loss mse --multi
python exp_interspecies_komatsuna.py --source-path $SOURCE_PATH --target-path $TARGET_PATH --split 2 -m trained_models/pretrained_cvppp/cvppp_pretrained_split_multi_2.h5 -o trained_models/inter_species/komatsuna/split2 --generator-loops 2 --loss mse --multi
#python exp_intraspecies_komatsuna.py --source-path $SOURCE_PATH --target-path $TARGET_PATH --split 3 -m trained_models/pretrained_cvppp/cvppp_pretrained_split_multi_3.h5 -o trained_models/inter_species/komatsuna/split3 --generator-loops 2 --loss mse --multi

