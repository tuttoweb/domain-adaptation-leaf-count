from keras import layers as LL
from keras.models import Model
from keras.optimizers import Adam
from keras.regularizers import l2,l1
from keras.utils import Progbar
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import EarlyStopping, ModelCheckpoint, CSVLogger
from keras.activations import sigmoid
from keras.losses import sparse_categorical_crossentropy,kullback_leibler_divergence as kld
import keras.backend as K
from Resnet50_counting_keras_vg import ResNet50
from gradient_reversal_keras_tf.flipGradientTF import GradientReversal
from scipy import stats
import os
import numpy as np
import tensorflow as tf

np.random.seed(40)
tf.set_random_seed(40)

FINAL_LAYER_CLASSIC = 1
FINAL_LAYER_MULTI   = 2

def final_layer_grad_reg(l=0.01):
    return lambda x : K.sum( K.mean( l*sigmoid(x)*(1-sigmoid(x)),axis=1))

def my_kld(y,y_hat):
    return kld(K.mean(y,axis=0), K.mean(y_hat,axis=0))

def sparse_softmax_cross_entropy_gen(y_true,y_pred):
    from tensorflow import losses as l

    y_true = K.cast(y_true,'int32')
    y_true = 1-K.concatenate([y_true,1-y_true],axis=0)

    return l.sparse_softmax_cross_entropy(y_true,y_pred)


def sparse_softmax_cross_entropy_disc(y_true,y_pred):
    from tensorflow import losses as l

    return l.sparse_softmax_cross_entropy(K.cast(y_true,'int32'),y_pred)


def wasserstein_loss(y_true, y_pred):
    return K.mean(y_true * y_pred)

def print_metrics(metrics,print_fn=print):
    print_fn("DiC ...: {} ({})".format(metrics['dic'][0],metrics['dic'][1]))
    print_fn("|DiC| .: {} ({})".format(metrics['abs_dic'][0],metrics['abs_dic'][1]))
    print_fn("MSE ...: {}".format(metrics['mse']))
    print_fn("% .....: {}".format(metrics['%']))
    print_fn("R2 ....: {}".format(metrics['r2']))

def save_predictions(Y,Y_hat,fname='pred.csv'):
    assert len(Y) == len(Y_hat)

    with open(fname,'w') as h:
        for i in range(len(Y)):
            h.write("{},{}\n".format(Y[i],Y_hat[i]))


def compute_metrics(Y,Y_hat):
    Y_hat = np.round(Y_hat)
    diff = Y - Y_hat

    #_, _, r_value, _,_ = stats.linregress(Y, Y_hat)

    res = {
        'dic': (diff.mean(),diff.std()),
        'abs_dic': ( np.abs(diff).mean(),np.abs(diff).std()),
        'mse' : (diff ** 2).mean(),
        '%' : np.mean(Y==Y_hat),
        'r2': 0#r_value ** 2
    }

    return res

class SumLayer(LL.Layer):

    def __init__(this, **kwargs):
        super(SumLayer, this).__init__(**kwargs)

    def build(this, input_shape):
        #this.bias = this.add_weight(name='bias',
        #                              shape=(1, ),
        #                              initializer='zeros',
        #                              trainable=True)
        super(SumLayer, this).build(input_shape)  # Be sure to call this somewhere!

    def call(this, x,**kwargs):
        return K.sum(x,axis=1,keepdims=True) # + this.bias

    def compute_output_shape(self, input_shape):
        assert input_shape and len(input_shape) >= 2
        assert input_shape[-1]
        output_shape = list(input_shape)
        output_shape[-1] = 1

        return tuple(output_shape)

class GDAN(object):
    Epochs = 50
    MiniBatchSize = 16
    LearingRate = {"discriminator":0.0002,"generator":0.0002,"regressor":0.001}
    WeightDecay = 0
    AdamParameters = {"beta1": 0.5, "beta2":0.999}
    L2RegulariserLambda = 0.02
    SigmoidRegulariserLambda = 0.02
    Sizes = [1024, 512]
    GanMaxEpochs = 30000
    SelfSupervised = False
    SelfSupervisedPatience = 10
    EndLayer = FINAL_LAYER_CLASSIC
    NumNodesLastLayer = 50

    WassersteinClipValue=0.01

    EarlyStoppingPatience = 300

    def __init__(this,input_size=(320,320,3),debug=False):
        this._input_size = input_size
        this.debug=debug

    def _get_resnet_network(this):
        model = ResNet50(include_top=False, weights='imagenet', input_shape=this._input_size)

        flatten = LL.Flatten(name='squeeze')(model.output)

        model = Model(inputs=model.input, outputs=flatten)

        return model

    def _get_regressor(this,input,trainable=True,**kwargs):
        x = LL.core.Dense(this.Sizes[0], activation='relu', name='regression_dense_1',trainable=trainable)(input)
        x = LL.core.Dense(this.Sizes[1], activation='relu', name='regression_dense_2',trainable=trainable,
                          activity_regularizer=l2(this.L2RegulariserLambda))(x)


        if (this.EndLayer == FINAL_LAYER_CLASSIC):
            output = LL.core.Dense(1,name='regression_output',trainable=trainable)(x)
        elif (this.EndLayer == FINAL_LAYER_MULTI):
            output = LL.core.Dense(this.NumNodesLastLayer, name='regression_output_multi',activation='sigmoid',
                                   activity_regularizer=final_layer_grad_reg(this.SigmoidRegulariserLambda),
                                   trainable=trainable)(x)

            if ("add_sum_layer" in kwargs):
                if (kwargs['add_sum_layer']==True):
                    output = SumLayer()(output)
        else:
            raise NotImplementedError('Not implemented final layer {}'.format(this.EndLayer))

        return output

    def _get_discriminator(this,discriminator_activation='sigmoid',input_size=2048):

        input = LL.Input((input_size,))

        #x = LL.core.Dense(500, name='disc_dense_1', kernel_initializer='he_uniform',kernel_regularizer=l2(0.01))(input)
        x = LL.core.Dense(this.Sizes[0], name='disc_dense_1',activity_regularizer=l2(2.5e-5))(input)
        x = LL.LeakyReLU(0.2)(x)
        #x = LL.core.Dense(250, name='disc_dense_2',kernel_initializer='he_uniform',kernel_regularizer=l2(0.01))(x)
        x = LL.core.Dense(this.Sizes[1], name='disc_dense_2', activity_regularizer=l2(2.5e-5))(x)
        x = LL.LeakyReLU(0.2)(x)
        output = LL.core.Dense(2,activation='linear', name='disc_output', kernel_initializer='he_uniform')(x)

        model = Model(inputs=input,outputs=output)

        return model



    def build_source_network(this):

        layers = this._get_resnet_network()
        x = layers.output

        output = this._get_regressor(x,add_sum_layer=True)

        model = Model(inputs=layers.input, outputs=output)

        if (this.debug):
            model.summary()

        this.source_model = model


    def pretrain_network(this,x_train,y_train,x_val,y_val,epochs=None,filename='source_output.h5'):


        this.source_model.compile(optimizer=Adam(lr=this.LearingRate['regressor']), loss='mse')

        x_aug = ImageDataGenerator(
            rotation_range=180,
            width_shift_range=0.1,
            height_shift_range=0.1,
            horizontal_flip=True,
            vertical_flip=True
        )

        x_aug.fit(x_train)

        callbacks = [EarlyStopping(patience=this.EarlyStoppingPatience, verbose=1)]

        if (epochs is None):
            epochs = this.Epochs

        if (filename is not None):
            callbacks.append(ModelCheckpoint(filename, verbose=1, save_best_only=True, save_weights_only=True, mode='min'))

        this.source_model.fit_generator(
            x_aug.flow(x_train, y_train, batch_size=this.MiniBatchSize),
            steps_per_epoch=812, epochs=epochs, validation_data=(x_val, y_val), callbacks=callbacks)

    def _get_suitable_discriminator_activation(this,loss):

        if (loss == "binary_crossentropy"):
            return "sigmoid"
        else:
            return "linear"

    def build_da_network(this,discriminator_loss="binary_crossentropy",reverse_gradient=False,include_reg_layers=None,src_pretrained_model='source_output.h5'):

        generator_model = this._get_resnet_network()
        generator_input = generator_model.input
        generator_output = generator_model.output

        regressor_output = this._get_regressor(generator_output,trainable=False,add_sum_layer=False)
        regressor_model = Model(inputs=generator_input, outputs=regressor_output)


        if (src_pretrained_model is not None):
            regressor_model.load_weights(src_pretrained_model,by_name=True)

        if (this.EndLayer==FINAL_LAYER_MULTI):
            regressor_model.compile(optimizer=Adam(lr=0.01*this.LearingRate['regressor']),loss=my_kld)

        discriminator_input_size = 2048 if include_reg_layers is None else this.Sizes[include_reg_layers-1]

        discriminator_model = this._get_discriminator(input_size=discriminator_input_size)

        discriminator_model.compile(optimizer=Adam(lr=this.LearingRate['discriminator'], decay=this.WeightDecay,
                                                   beta_1=this.AdamParameters["beta1"],
                                                   beta_2=this.AdamParameters["beta2"]),
                                    loss=sparse_softmax_cross_entropy_disc, metrics=[ "accuracy"])

        discriminator_model.trainable=False

        ##creating combined model
        source_features_input = LL.Input((discriminator_input_size,))

        combined_output = generator_model(generator_input)
        combined_output = LL.Concatenate(axis=0)([source_features_input,combined_output])
        combined_output = discriminator_model(combined_output)
        #combined_output = LL.Lambda(lambda x: tf.Print(x, [tf.shape(x)], "eccolo: ", summarize=100))(combined_output)



        gan_model = Model (inputs=[generator_input,source_features_input],outputs=[combined_output])


        #TODO: implement supervised cost

        gan_model.compile(optimizer=Adam(lr=this.LearingRate['discriminator'], decay=this.WeightDecay,
                                                   beta_1=this.AdamParameters["beta1"],
                                                   beta_2=this.AdamParameters["beta2"]),
                                    loss=sparse_softmax_cross_entropy_gen)#, metrics=["accuracy"])

        this.discriminator = discriminator_model
        this.generator = generator_model
        this.gan = gan_model
        this.regressor =this.source_model = regressor_model








    def train_da(this,x_source,train,validation=None,clip=False,generator_iterations=1,output_folder="."):
        if (type(train)=='list'):
            x_train = train[0]
            y_train = train[1] if len(train)>0 else None

        else:
            x_train = train

        if (validation is not None):
            x_val = validation[0]
            y_val = validation[1]

        batch_size = int(np.floor(this.MiniBatchSize / 2))
        batches = int(np.ceil(x_train.shape[0] / batch_size))

        x_aug = ImageDataGenerator(
            rotation_range=180,
            width_shift_range=0.1,
            height_shift_range=0.1,
            horizontal_flip=True,
            vertical_flip=True).flow(x_train,batch_size=batch_size)

        loss_names = ["loss", "Discriminator_loss_bce", "Discriminator_loss_acc", "Generator_loss_bce", "Generator_loss_acc",
                "val_loss","val_mse"]

        cl = CSVLogger(output_folder + '/training.csv')
        cl.on_train_begin()



        progress_bar = Progbar(target=batches*batch_size)

        x_real_features = this.generator.predict(x_source, batch_size=this.MiniBatchSize)
        y_real_output   = this.regressor.predict(x_source, batch_size=this.MiniBatchSize)

        total_loss = {n: [] for n in loss_names}

        not_improving = 0

        best_val_loss=np.Inf

        #self_supervised_y_train = self_supervised_y_val = None

        for epoch in range(1,this.GanMaxEpochs+1):
            print ("Epoch {}/{}".format(epoch,this.GanMaxEpochs))

            epoch_loss = {n: [] for n in loss_names}
            epoch_loss_list = []



            for batch in range(batches):

                x_batch = next(x_aug)

                rnd_idx = np.random.permutation(x_real_features.shape[0])[0:x_batch.shape[0]];
                x_real_features_batch = x_real_features[rnd_idx, ...]

                real_pred = np.zeros((x_batch.shape[0], 1),dtype='int32')
                fake_pred = np.ones((x_batch.shape[0], 1),dtype='int32')

                y_pred = np.concatenate([real_pred,fake_pred],axis=0)

                # Train generator
                for i in range(generator_iterations):
                    h=this.gan.train_on_batch([x_batch,x_real_features_batch],real_pred)#,epochs=1,verbose=0)


                    g_loss_bce = h#[0]
                    #g_loss_acc = h[1]

                    epoch_loss['Generator_loss_bce'].append(g_loss_bce)
                    #epoch_loss['Generator_loss_acc'].append(g_loss_acc)

                # Train discriminator

                x_fake_features_batch = this.generator.predict(x_batch)

                x_features = np.concatenate([x_real_features_batch,x_fake_features_batch])

                h_fake=this.discriminator.train_on_batch(x_features,y_pred)

                if (this.EndLayer == FINAL_LAYER_MULTI):
                    # Train Regressor
                    h_reg = this.regressor.train_on_batch(x_batch,y_real_output[rnd_idx])


                if (clip):
                    # Clip critic weights
                    for l in this.discriminator.layers:
                        weights = l.get_weights()
                        weights = [np.clip(w, -this.WassersteinClipValue, this.WassersteinClipValue) for w in weights]
                        l.set_weights(weights)

                # # print(np.mean(np.abs(dense_aft2 - dense_aft)))

                d_loss_bce = h_fake[0]#np.mean([h_real[0], h_fake[0]])
                d_loss_acc = h_fake[1]#np.mean([h_real[1], h_fake[1]])
                epoch_loss['Discriminator_loss_bce'].append(d_loss_bce)
                epoch_loss['Discriminator_loss_acc'].append(d_loss_acc)


                # progbar update
                progress_bar.update( (batch+1)*batch_size)


            for n in loss_names:
                epoch_loss_list.append((n, np.mean(epoch_loss[n])))
                total_loss[n].append(np.mean(epoch_loss[n]))

            if (validation is not None):
                val_pred = this.regressor.predict(x_val, batch_size=this.MiniBatchSize)
                val_metric = compute_metrics(Y=y_val, Y_hat=val_pred.sum(axis=1))

                total_loss['val_mse'].append(val_metric['mse'])

                print("Validation MSE: {}".format(val_metric['mse']))

                if ( (epoch==1) or (total_loss['val_mse'][-1]<best_val_loss)):
                    this.generator.save_weights(os.path.join(output_folder,"generator.h5"))
                    this.discriminator.save_weights(os.path.join(output_folder, "discriminator.h5"))
                    this.regressor.save_weights(os.path.join(output_folder, "regressor.h5"))
                    not_improving=0
                    best_val_loss=total_loss['val_mse'][-1]

                    #self_supervised_y_train = this.regressor.predict(x_train)
                    #self_supervised_y_val = this.regressor.predict(x_val)

                    print("dense_1: {}".format( this.regressor.get_layer('regression_dense_1').get_weights()[0].sum()))
                    print("res5c_branch2c: {}".format(this.regressor.get_layer('res5c_branch2c').get_weights()[0].sum()))

                    print("Val MSE improved")
                else:
                    not_improving+=1
                    #if (this.SelfSupervised):
                    #    if ( (not_improving % this.SelfSupervisedPatience) == 0):
                    #        print ("Self Supervision in progress....")
                    #        self_supervised_y_train = self_supervised_y_train [np.random.permutation(self_supervised_y_train.shape[0])]
                    #        this.pretrain_network(x_train,np.round(self_supervised_y_train),x_val,self_supervised_y_val,1,None)



            logs = {l: total_loss[l][-1] for l in loss_names}

            cl.model = this.discriminator
            cl.model.stop_training = False
            cl.on_epoch_end(epoch, logs)

            if (not_improving>=this.EarlyStoppingPatience):
                break

