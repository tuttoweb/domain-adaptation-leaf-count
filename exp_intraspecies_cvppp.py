from network import * #GDAN,compute_metrics,print_metrics,wasserstein_loss
from loader import CVPPP17Loader,MultiModalDataLoader
import pandas as pd
import numpy as np
import os
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--source-path',dest='source_path',action='store',type=str,required=True)
parser.add_argument('--target-path',dest='target_path',action='store',type=str,required=True)
parser.add_argument('--patience',dest='patience',action='store',type=int,default=10)
parser.add_argument('--split','-s',dest='split',action='store',type=int,default=0)
parser.add_argument('--generator-loops',dest='gen_lop',action='store',type=int,default=1)
parser.add_argument('--test',dest='test',action='store_true')
parser.add_argument('--output','-o',dest='output',action='store',type=str,default="model.h5")
parser.add_argument('--model','-m',dest='model',action='store',type=str,default=None)
parser.add_argument('--resume',dest='resume',action='store_true')
parser.add_argument('--loss','-l',dest='loss',action='store',choices=['ce','w','mse'],default='ce')
parser.add_argument('--regression-layer',dest='reg_layer',action='store',type=int,choices=[None,1,2],default=None)
parser.add_argument('--gr',dest='gr',action='store_true')
parser.add_argument('--multi',dest='multi',action='store_true')

args = parser.parse_args()

print(args)

loss = 'binary_crossentropy'
clip_weights=False
if (args.loss =='w'):
    loss = wasserstein_loss
    clip_weights = True
elif (args.loss == 'mse'):
    loss = 'mse'

model = GDAN()
model.MiniBatchSize=32

if (args.multi):
    model.EndLayer = FINAL_LAYER_MULTI
else:
    model.EndLayer = FINAL_LAYER_CLASSIC

model.build_da_network(src_pretrained_model=args.model,discriminator_loss=loss,reverse_gradient=args.gr,
                       include_reg_layers=args.reg_layer)


model.discriminator.summary()
model.generator.summary()
model.gan.summary()
model.regressor.summary()

if (args.resume):
    print ("Resuming training...")
    model.generator.load_weights(os.path.join(args.output, "generator.h5"))
    model.discriminator.load_weights(os.path.join(args.output, "discriminator.h5"))
    model.regressor.load_weights(os.path.join(args.output, "regressor.h5"))

trg_loader = CVPPP17Loader(args.target_path,None,a3=False,autocontrast=True,normalise=True)
src_loader = MultiModalDataLoader(args.source_path,rescale=(320,320),autocontrast=True,normalise=True)

if (args.test):
    X_test, Y_test = trg_loader.get_data("testing", args.split)
    model.build_source_network()
    model.source_model.load_weights(args.model,by_name=True)

    Y_hat = model.source_model.predict(X_test)
    Y_hat = np.round(Y_hat[:,0])

    test_metric = compute_metrics(Y_test,Y_hat)
    print_metrics(test_metric)

    with open(args.output,'w') as h:
        Y_hat = np.round(Y_hat)

        for i in range(len(Y_hat)):
            h.write("{},{}\n".format(Y_test[i],Y_hat[i]))

    data = pd.DataFrame([Y_test,Y_hat]).transpose()
    data.to_csv(args.output)

    print ("[DONE]")



else:
    X_src,_, _, _ = src_loader.get_data("training",args.split)
    X_trg_train,Y_trg_train, X_trg_val,Y_trg_val = trg_loader.get_data("training",args.split)

    model.train_da(X_src[0],X_trg_train,[X_trg_val,Y_trg_val],clip=clip_weights,output_folder=args.output,
                   generator_iterations=args.gen_lop)

    model.regressor.load_weights(os.path.join(args.output,'regressor.h5'),by_name=True)

    Y_train_hat = model.regressor.predict(X_trg_train)
    Y_val_hat  = model.regressor.predict(X_trg_val)

    train_metric = compute_metrics(Y_trg_train,Y_train_hat[:,0])
    test_metric = compute_metrics(Y_trg_val,Y_val_hat[:,0])

    print("dense_1: {}".format(model.regressor.get_layer('regression_dense_1').get_weights()[0].sum()))
    print("res5c_branch2c: {}".format(model.regressor.get_layer('res5c_branch2c').get_weights()[0].sum()))

    print(">>> TRAINING ERROR")
    print_metrics(train_metric)

    print("")
    print(">>> VALIDATION ERROR")
    print_metrics(test_metric)


