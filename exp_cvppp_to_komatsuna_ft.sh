#!/bin/bash

export CUDA_VISIBLE_DEVICES=0

TRAINING_PATH="../RDS/14-Frontiers/komatsuna.mat"

python exp_cvppp_to_komatsuna_ft.py --dataset-path $TRAINING_PATH --model trained_models/pretrained_cvppp/cvppp_pretrained_split_multi_2.h5 --multi -o trained_models/cvppp_2_km/multi_ft_0.h5

