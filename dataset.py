import numpy as np
import os
import re
import csv
import glob
import os.path as path
import scipy.misc as misc
import pandas as pd
import keras.backend as K
from keras.preprocessing.image import *
from PIL import Image,ImageOps
from scipy import linalg


def rescale(x,resize):
    xr = np.zeros((x.shape[0],resize[0],resize[1],x.shape[-1]))
    for k in range(x.shape[0]):
        I  = Image.fromarray(x[k,:,:,:].astype('uint8'))

        xr[k,...] = np.asarray(I.resize(resize,Image.BICUBIC))
    return xr

def normalise(x):
    m = x.min()
    M = x.max()

    h = (M-m)/2

    return (x / h)-1


def autocontrast(x):
    for k in range(x.shape[0]):
        I  = Image.fromarray(x[k,:,:,:].astype('uint8'))
        I  = ImageOps.autocontrast(I)
        #y  = np.asarray(I).transpose((2,0,1))
        x[k,...] = np.asarray(I)
    return x

def dataset_augmentation(X,rotations=range(0,360,90),flip=True):
    X = X.transpose((2,3,1,0))

    N = X.shape[-1] * len(rotations) * (3 if flip else 1)

    X_aug = np.zeros( ( X.shape[0],X.shape[1],X.shape[2], N) )

    flips = (range(3) if flip else range(1))

    k = 0

    for f in flips:
        for theta in rotations:
            for i in range(X.shape[-1]):
                mode = None

                img = X[:, :, :, i]
                prev = img.dtype


                if (theta!=0):

                    if img.shape[-1]==1:
                        mode = 'L'
                        img = img[:,:,0]

                    img = np.asarray(img, dtype='uint8')
                    I = Image.fromarray(img,mode=mode)
                    img = np.asarray(I.rotate(theta),dtype=prev)

                    if (len(img.shape)<3):
                        img = img.reshape((img.shape[0],img.shape[1],1))

                if f==1: # h flip
                    img = img[-1::-1,:,:]
                elif f==2: # v flip
                    img = img[:,-1::-1, :]

                X_aug[:,:,:,k] = img

                k+=1

    return X_aug.transpose((3,2,0,1))


def get_filename(p):
    _, t = path.split(p)
    return t


def get_last_dir(p):
    h, _ = path.split(p)
    _, t = path.split(h)
    return t[:2]


def get_data_masks(split_load, basepath='CVPPP2017_LCC_training/TrainingSplits/'):
        ###############################
        # Getting images (x data)	  #
        ###############################
    imgname_train_A1 = np.array([glob.glob(path.join(basepath, 'A1' + str(h) + '/*fg.png')) for h in split_load[0]])
    imgname_train_A2 = np.array([glob.glob(path.join(basepath, 'A2' + str(h) + '/*fg.png')) for h in split_load[0]])
    imgname_train_A3 = np.array([glob.glob(path.join(basepath, 'A3' + str(h) + '/*fg.png')) for h in split_load[0]])
    imgname_train_A4 = np.array([glob.glob(path.join(basepath, 'A4' + str(h) + '/*fg.png')) for h in split_load[0]])

    imgname_val_A1 = np.array([glob.glob(path.join(basepath, 'A1' + str(split_load[1])) + '/*fg.png')])
    imgname_val_A2 = np.array([glob.glob(path.join(basepath, 'A2' + str(split_load[1])) + '/*fg.png')])
    imgname_val_A3 = np.array([glob.glob(path.join(basepath, 'A3' + str(split_load[1])) + '/*fg.png')])
    imgname_val_A4 = np.array([glob.glob(path.join(basepath, 'A4' + str(split_load[1])) + '/*fg.png')])

    imgname_test_A1 = np.array([glob.glob(path.join(basepath, 'A1' + str(split_load[2])) + '/*fg.png')])
    imgname_test_A2 = np.array([glob.glob(path.join(basepath, 'A2' + str(split_load[2])) + '/*fg.png')])
    imgname_test_A3 = np.array([glob.glob(path.join(basepath, 'A3' + str(split_load[2])) + '/*fg.png')])
    imgname_test_A4 = np.array([glob.glob(path.join(basepath, 'A4' + str(split_load[2])) + '/*fg.png')])

    filelist_train_A1 = list(np.sort(imgname_train_A1.flat))
    filelist_train_A2 = list(np.sort(imgname_train_A2.flat))
    filelist_train_A3 = list(np.sort(imgname_train_A3.flat))
    filelist_train_A4 = list(np.sort(imgname_train_A4.flat))


    filelist_train_A1_img = np.array([np.array(get_filename(filelist_train_A1[h])) for h in range(len(filelist_train_A1))])
    filelist_train_A2_img = np.array([np.array(get_filename(filelist_train_A2[h])) for h in range(len(filelist_train_A2))])
    filelist_train_A3_img = np.array([np.array(get_filename(filelist_train_A3[h])) for h in range(len(filelist_train_A3))])
    filelist_train_A4_img = np.array([np.array(get_filename(filelist_train_A4[h])) for h in range(len(filelist_train_A4))])

    filelist_train_A1_set = np.array([np.array(get_last_dir(filelist_train_A1[h])) for h in range(len(filelist_train_A1))])
    filelist_train_A2_set = np.array([np.array(get_last_dir(filelist_train_A2[h])) for h in range(len(filelist_train_A2))])
    filelist_train_A3_set = np.array([np.array(get_last_dir(filelist_train_A3[h])) for h in range(len(filelist_train_A3))])
    filelist_train_A4_set = np.array([np.array(get_last_dir(filelist_train_A4[h])) for h in range(len(filelist_train_A4))])

    filelist_val_A1 = list(np.sort(imgname_val_A1.flat))
    filelist_val_A2 = list(np.sort(imgname_val_A2.flat))
    filelist_val_A3 = list(np.sort(imgname_val_A3.flat))
    filelist_val_A4 = list(np.sort(imgname_val_A4.flat))
    filelist_val_A1_img = np.array([np.array(get_filename(filelist_val_A1[h])) for h in range(len(filelist_val_A1))])
    filelist_val_A2_img = np.array([np.array(get_filename(filelist_val_A2[h])) for h in range(len(filelist_val_A2))])
    filelist_val_A3_img = np.array([np.array(get_filename(filelist_val_A3[h])) for h in range(len(filelist_val_A3))])
    filelist_val_A4_img = np.array([np.array(get_filename(filelist_val_A4[h])) for h in range(len(filelist_val_A4))])
    filelist_val_A1_set = np.array([np.array(get_last_dir(filelist_val_A1[h])) for h in range(len(filelist_val_A1))])
    filelist_val_A2_set = np.array([np.array(get_last_dir(filelist_val_A2[h])) for h in range(len(filelist_val_A2))])
    filelist_val_A3_set = np.array([np.array(get_last_dir(filelist_val_A3[h])) for h in range(len(filelist_val_A3))])
    filelist_val_A4_set = np.array([np.array(get_last_dir(filelist_val_A4[h])) for h in range(len(filelist_val_A4))])

    filelist_test_A1 = list(np.sort(imgname_test_A1.flat))
    filelist_test_A2 = list(np.sort(imgname_test_A2.flat))
    filelist_test_A3 = list(np.sort(imgname_test_A3.flat))
    filelist_test_A4 = list(np.sort(imgname_test_A4.flat))
    filelist_test_A1_img = np.array([np.array(get_filename(filelist_test_A1[h])) for h in range(len(filelist_test_A1))])
    filelist_test_A2_img = np.array([np.array(get_filename(filelist_test_A2[h])) for h in range(len(filelist_test_A2))])
    filelist_test_A3_img = np.array([np.array(get_filename(filelist_test_A3[h])) for h in range(len(filelist_test_A3))])
    filelist_test_A4_img = np.array([np.array(get_filename(filelist_test_A4[h])) for h in range(len(filelist_test_A4))])
    filelist_test_A1_set = np.array([np.array(get_last_dir(filelist_test_A1[h])) for h in range(len(filelist_test_A1))])
    filelist_test_A2_set = np.array([np.array(get_last_dir(filelist_test_A2[h])) for h in range(len(filelist_test_A2))])
    filelist_test_A3_set = np.array([np.array(get_last_dir(filelist_test_A3[h])) for h in range(len(filelist_test_A3))])
    filelist_test_A4_set = np.array([np.array(get_last_dir(filelist_test_A4[h])) for h in range(len(filelist_test_A4))])

    # Read image names into np array train
    x_train_A1 = np.array([np.array(misc.imread(fname)) for fname in filelist_train_A1])
    x_train_A2 = np.array([np.array(misc.imread(fname)) for fname in filelist_train_A2])
    x_train_A3 = np.array([np.array(Image.open(fname)) for fname in filelist_train_A3])
    x_train_A3 = x_train_A3[:,:,:,np.newaxis]; x_train_A3 = np.repeat(x_train_A3,3,3)
    x_train_A4 = np.array([np.array(Image.open(fname)) for fname in filelist_train_A4])
    x_train_A4 = x_train_A4[:,:,:,np.newaxis]; x_train_A4 = np.repeat(x_train_A4,3,3)

    # Read image names into np array validation
    x_val_A1 = np.array([np.array(misc.imread(fname)) for fname in filelist_val_A1])
    #x_val_A1 = np.delete(x_va283l_A1, 3, 3)
    x_val_A2 = np.array([np.array(misc.imread(fname)) for fname in filelist_val_A2])
    #x_val_A2 = np.delete(x_val_A2, 3, 3)
    x_val_A3 = np.array([np.array(Image.open(fname)) for fname in filelist_val_A3])
    x_val_A3 = x_val_A3[:,:,:,np.newaxis]; x_val_A3 = np.repeat(x_val_A3,3,3)
    x_val_A4 = np.array([np.array(Image.open(fname)) for fname in filelist_val_A4])
    x_val_A4 = x_val_A4[:,:,:,np.newaxis]; x_val_A4 = np.repeat(x_val_A4,3,3)

    # Read image names into np array test
    x_test_A1 = np.array([np.array(misc.imread(fname)) for fname in filelist_test_A1])
    x_test_A2 = np.array([np.array(Image.open(fname)) for fname in filelist_test_A2])
    x_test_A2 = x_test_A2[:,:,:,np.newaxis]; x_test_A2 = np.repeat(x_test_A2,3,3)
    x_test_A3 = np.array([np.array(Image.open(fname)) for fname in filelist_test_A3])
    x_test_A3 = x_test_A3[:,:,:,np.newaxis]; x_test_A3 = np.repeat(x_test_A3,3,3)
    x_test_A4 = np.array([np.array(Image.open(fname)) for fname in filelist_test_A4])
    x_test_A4 = x_test_A4[:,:,:,np.newaxis]; x_test_A4 = np.repeat(x_test_A4,3,3)

    x_train_res_A1 = np.array([misc.imresize(x_train_A1[i], [320, 320, 3]) for i in range(0, len(x_train_A1))])
    x_train_res_A2 = np.array([misc.imresize(x_train_A2[i], [320, 320, 3]) for i in range(0, len(x_train_A2))])
    x_train_res_A3 = np.array([misc.imresize(x_train_A3[i], [320, 320, 3]) for i in range(0, len(x_train_A3))])
    x_train_res_A4 = np.array([misc.imresize(x_train_A4[i], [320, 320, 3]) for i in range(0, len(x_train_A4))])

    x_val_res_A1 = np.array([misc.imresize(x_val_A1[i], [320, 320, 3]) for i in range(0, len(x_val_A1))])
    x_val_res_A2 = np.array([misc.imresize(x_val_A2[i], [320, 320, 3]) for i in range(0, len(x_val_A2))])
    x_val_res_A3 = np.array([misc.imresize(x_val_A3[i], [320, 320, 3]) for i in range(0, len(x_val_A3))])
    x_val_res_A4 = np.array([misc.imresize(x_val_A4[i], [320, 320, 3]) for i in range(0, len(x_val_A4))])

    x_test_res_A1 = np.array([misc.imresize(x_test_A1[i], [320, 320, 3]) for i in range(0, len(x_test_A1))])
    x_test_res_A2 = np.array([misc.imresize(x_test_A2[i], [320, 320, 3]) for i in range(0, len(x_test_A2))])
    x_test_res_A3 = np.array([misc.imresize(x_test_A3[i], [320, 320, 3]) for i in range(0, len(x_test_A3))])
    x_test_res_A4 = np.array([misc.imresize(x_test_A4[i], [320, 320, 3]) for i in range(0, len(x_test_A4))])

    x_train_all = np.concatenate((x_train_res_A1, x_train_res_A2, x_train_res_A3, x_train_res_A4), axis=0)
    x_val_all = np.concatenate((x_val_res_A1, x_val_res_A2, x_val_res_A3, x_val_res_A4), axis=0)
    x_test_all = np.concatenate((x_test_res_A1, x_test_res_A2, x_test_res_A3, x_test_res_A4), axis=0)



    # Concatenate the image names
    x_train_img = np.concatenate(
        (filelist_train_A1_img, filelist_train_A2_img, filelist_train_A3_img, filelist_train_A4_img), axis=0)
    x_val_img = np.concatenate((filelist_val_A1_img, filelist_val_A2_img, filelist_val_A3_img, filelist_val_A4_img), axis=0)
    x_test_img = np.concatenate((filelist_test_A1_img, filelist_test_A2_img, filelist_test_A3_img, filelist_test_A4_img),
                                axis=0)

    x_train_set = np.concatenate(
        (filelist_train_A1_set, filelist_train_A2_set, filelist_train_A3_set, filelist_train_A4_set), axis=0)
    x_val_set = np.concatenate((filelist_val_A1_set, filelist_val_A2_set, filelist_val_A3_set, filelist_val_A4_set), axis=0)
    x_test_set = np.concatenate((filelist_test_A1_set, filelist_test_A2_set, filelist_test_A3_set, filelist_test_A4_set),
                                axis=0)



    return x_train_all, x_val_all, x_test_all, x_train_set, x_val_set, x_test_set, x_train_img, x_val_img, x_test_img


def get_data(split_load, basepath='CVPPP2017_LCC_training/TrainingSplits/'):

    imgname_train_A1 = np.array([glob.glob(path.join(basepath, 'A1' + str(h) + '/*rgb.png')) for h in split_load[0]])
    imgname_train_A2 = np.array([glob.glob(path.join(basepath, 'A2' + str(h) + '/*rgb.png')) for h in split_load[0]])
    imgname_train_A3 = np.array([glob.glob(path.join(basepath, 'A3' + str(h) + '/*rgb.png')) for h in split_load[0]])
    imgname_train_A4 = np.array([glob.glob(path.join(basepath, 'A4' + str(h) + '/*rgb.png')) for h in split_load[0]])

    imgname_val_A1 = np.array([glob.glob(path.join(basepath, 'A1' + str(split_load[1])) + '/*rgb.png')])
    imgname_val_A2 = np.array([glob.glob(path.join(basepath, 'A2' + str(split_load[1])) + '/*rgb.png')])
    imgname_val_A3 = np.array([glob.glob(path.join(basepath, 'A3' + str(split_load[1])) + '/*rgb.png')])
    imgname_val_A4 = np.array([glob.glob(path.join(basepath, 'A4' + str(split_load[1])) + '/*rgb.png')])

    imgname_test_A1 = np.array([glob.glob(path.join(basepath, 'A1' + str(split_load[2])) + '/*rgb.png')])
    imgname_test_A2 = np.array([glob.glob(path.join(basepath, 'A2' + str(split_load[2])) + '/*rgb.png')])
    imgname_test_A3 = np.array([glob.glob(path.join(basepath, 'A3' + str(split_load[2])) + '/*rgb.png')])
    imgname_test_A4 = np.array([glob.glob(path.join(basepath, 'A4' + str(split_load[2])) + '/*rgb.png')])

    filelist_train_A1 = list(np.sort(imgname_train_A1.flat))
    filelist_train_A2 = list(np.sort(imgname_train_A2.flat))
    filelist_train_A3 = list(np.sort(imgname_train_A3.flat))
    filelist_train_A4 = list(np.sort(imgname_train_A4.flat))


    filelist_train_A1_img = np.array([np.array(get_filename(filelist_train_A1[h])) for h in range(len(filelist_train_A1))])
    filelist_train_A2_img = np.array([np.array(get_filename(filelist_train_A2[h])) for h in range(len(filelist_train_A2))])
    filelist_train_A3_img = np.array([np.array(get_filename(filelist_train_A3[h])) for h in range(len(filelist_train_A3))])
    filelist_train_A4_img = np.array([np.array(get_filename(filelist_train_A4[h])) for h in range(len(filelist_train_A4))])

    filelist_train_A1_set = np.array([np.array(get_last_dir(filelist_train_A1[h])) for h in range(len(filelist_train_A1))])
    filelist_train_A2_set = np.array([np.array(get_last_dir(filelist_train_A2[h])) for h in range(len(filelist_train_A2))])
    filelist_train_A3_set = np.array([np.array(get_last_dir(filelist_train_A3[h])) for h in range(len(filelist_train_A3))])
    filelist_train_A4_set = np.array([np.array(get_last_dir(filelist_train_A4[h])) for h in range(len(filelist_train_A4))])

    filelist_val_A1 = list(np.sort(imgname_val_A1.flat))
    filelist_val_A2 = list(np.sort(imgname_val_A2.flat))
    filelist_val_A3 = list(np.sort(imgname_val_A3.flat))
    filelist_val_A4 = list(np.sort(imgname_val_A4.flat))
    filelist_val_A1_img = np.array([np.array(get_filename(filelist_val_A1[h])) for h in range(len(filelist_val_A1))])
    filelist_val_A2_img = np.array([np.array(get_filename(filelist_val_A2[h])) for h in range(len(filelist_val_A2))])
    filelist_val_A3_img = np.array([np.array(get_filename(filelist_val_A3[h])) for h in range(len(filelist_val_A3))])
    filelist_val_A4_img = np.array([np.array(get_filename(filelist_val_A4[h])) for h in range(len(filelist_val_A4))])
    filelist_val_A1_set = np.array([np.array(get_last_dir(filelist_val_A1[h])) for h in range(len(filelist_val_A1))])
    filelist_val_A2_set = np.array([np.array(get_last_dir(filelist_val_A2[h])) for h in range(len(filelist_val_A2))])
    filelist_val_A3_set = np.array([np.array(get_last_dir(filelist_val_A3[h])) for h in range(len(filelist_val_A3))])
    filelist_val_A4_set = np.array([np.array(get_last_dir(filelist_val_A4[h])) for h in range(len(filelist_val_A4))])

    filelist_test_A1 = list(np.sort(imgname_test_A1.flat))
    filelist_test_A2 = list(np.sort(imgname_test_A2.flat))
    filelist_test_A3 = list(np.sort(imgname_test_A3.flat))
    filelist_test_A4 = list(np.sort(imgname_test_A4.flat))
    filelist_test_A1_img = np.array([np.array(get_filename(filelist_test_A1[h])) for h in range(len(filelist_test_A1))])
    filelist_test_A2_img = np.array([np.array(get_filename(filelist_test_A2[h])) for h in range(len(filelist_test_A2))])
    filelist_test_A3_img = np.array([np.array(get_filename(filelist_test_A3[h])) for h in range(len(filelist_test_A3))])
    filelist_test_A4_img = np.array([np.array(get_filename(filelist_test_A4[h])) for h in range(len(filelist_test_A4))])
    filelist_test_A1_set = np.array([np.array(get_last_dir(filelist_test_A1[h])) for h in range(len(filelist_test_A1))])
    filelist_test_A2_set = np.array([np.array(get_last_dir(filelist_test_A2[h])) for h in range(len(filelist_test_A2))])
    filelist_test_A3_set = np.array([np.array(get_last_dir(filelist_test_A3[h])) for h in range(len(filelist_test_A3))])
    filelist_test_A4_set = np.array([np.array(get_last_dir(filelist_test_A4[h])) for h in range(len(filelist_test_A4))])

    # Read image names into np array train
    x_train_A1 = np.array([np.array(misc.imread(fname)) for fname in filelist_train_A1])
    x_train_A1 = np.delete(x_train_A1, 3, 3)
    x_train_A2 = np.array([np.array(misc.imread(fname)) for fname in filelist_train_A2])
    x_train_A2 = np.delete(x_train_A2, 3, 3)
    x_train_A3 = np.array([np.array(Image.open(fname)) for fname in filelist_train_A3])
    x_train_A4 = np.array([np.array(Image.open(fname)) for fname in filelist_train_A4])


    # Read image names into np array validation
    x_val_A1 = np.array([np.array(misc.imread(fname)) for fname in filelist_val_A1])
    x_val_A1 = np.delete(x_val_A1, 3, 3)
    x_val_A2 = np.array([np.array(misc.imread(fname)) for fname in filelist_val_A2])
    x_val_A2 = np.delete(x_val_A2, 3, 3)
    x_val_A3 = np.array([np.array(Image.open(fname)) for fname in filelist_val_A3])
    x_val_A4 = np.array([np.array(Image.open(fname)) for fname in filelist_val_A4])

    # Read image names into np array test
    x_test_A1 = np.array([np.array(misc.imread(fname)) for fname in filelist_test_A1])
    x_test_A1 = np.delete(x_test_A1, 3, 3)
    x_test_A2 = np.array([np.array(Image.open(fname)) for fname in filelist_test_A2])
    x_test_A2 = np.delete(x_test_A2, 3, 3)
    x_test_A3 = np.array([np.array(Image.open(fname)) for fname in filelist_test_A3])
    x_test_A4 = np.array([np.array(Image.open(fname)) for fname in filelist_test_A4])

    x_train_res_A1 = np.array([misc.imresize(x_train_A1[i], [320, 320, 3]) for i in range(0, len(x_train_A1))])
    x_train_res_A2 = np.array([misc.imresize(x_train_A2[i], [320, 320, 3]) for i in range(0, len(x_train_A2))])
    x_train_res_A3 = np.array([misc.imresize(x_train_A3[i], [320, 320, 3]) for i in range(0, len(x_train_A3))])
    x_train_res_A4 = np.array([misc.imresize(x_train_A4[i], [320, 320, 3]) for i in range(0, len(x_train_A4))])

    x_val_res_A1 = np.array([misc.imresize(x_val_A1[i], [320, 320, 3]) for i in range(0, len(x_val_A1))])
    x_val_res_A2 = np.array([misc.imresize(x_val_A2[i], [320, 320, 3]) for i in range(0, len(x_val_A2))])
    x_val_res_A3 = np.array([misc.imresize(x_val_A3[i], [320, 320, 3]) for i in range(0, len(x_val_A3))])
    x_val_res_A4 = np.array([misc.imresize(x_val_A4[i], [320, 320, 3]) for i in range(0, len(x_val_A4))])

    x_test_res_A1 = np.array([misc.imresize(x_test_A1[i], [320, 320, 3]) for i in range(0, len(x_test_A1))])
    x_test_res_A2 = np.array([misc.imresize(x_test_A2[i], [320, 320, 3]) for i in range(0, len(x_test_A2))])
    x_test_res_A3 = np.array([misc.imresize(x_test_A3[i], [320, 320, 3]) for i in range(0, len(x_test_A3))])
    x_test_res_A4 = np.array([misc.imresize(x_test_A4[i], [320, 320, 3]) for i in range(0, len(x_test_A4))])

    x_train_all = np.concatenate((x_train_res_A1, x_train_res_A2, x_train_res_A3, x_train_res_A4), axis=0)
    x_val_all = np.concatenate((x_val_res_A1, x_val_res_A2, x_val_res_A3, x_val_res_A4), axis=0)
    x_test_all = np.concatenate((x_test_res_A1, x_test_res_A2, x_test_res_A3, x_test_res_A4), axis=0)

    # Histogram stretching
    for h in range(0, len(x_train_all)):
        x_img = x_train_all[h]
        x_img_pil = Image.fromarray(x_img)
        x_img_pil = ImageOps.autocontrast(x_img_pil)
        x_img_ar = np.array(x_img_pil)
        x_train_all[h] = x_img_ar

    for h in range(0, len(x_val_all)):
        x_img = x_val_all[h]
        x_img_pil = Image.fromarray(x_img)
        x_img_pil = ImageOps.autocontrast(x_img_pil)
        x_img_ar = np.array(x_img_pil)
        x_val_all[h] = x_img_ar

    for h in range(0, len(x_test_all)):
        x_img = x_test_all[h]
        x_img_pil = Image.fromarray(x_img)
        x_img_pil = ImageOps.autocontrast(x_img_pil)
        x_img_ar = np.array(x_img_pil)
        x_test_all[h] = x_img_ar

    # Concatenate the image names
    x_train_img = np.concatenate(
        (filelist_train_A1_img, filelist_train_A2_img, filelist_train_A3_img, filelist_train_A4_img), axis=0)
    x_val_img = np.concatenate((filelist_val_A1_img, filelist_val_A2_img, filelist_val_A3_img, filelist_val_A4_img), axis=0)
    x_test_img = np.concatenate((filelist_test_A1_img, filelist_test_A2_img, filelist_test_A3_img, filelist_test_A4_img),
                                axis=0)

    x_train_set = np.concatenate(
        (filelist_train_A1_set, filelist_train_A2_set, filelist_train_A3_set, filelist_train_A4_set), axis=0)
    x_val_set = np.concatenate((filelist_val_A1_set, filelist_val_A2_set, filelist_val_A3_set, filelist_val_A4_set), axis=0)
    x_test_set = np.concatenate((filelist_test_A1_set, filelist_test_A2_set, filelist_test_A3_set, filelist_test_A4_set),
                                axis=0)

    ###############################
    # Getting targets (y data)    #
    ###############################
    counts_A1 = np.array([glob.glob(path.join(basepath, 'A1.xlsx'))])
    counts_A2 = np.array([glob.glob(path.join(basepath, 'A2.xlsx'))])
    counts_A3 = np.array([glob.glob(path.join(basepath, 'A3.xlsx'))])
    counts_A4 = np.array([glob.glob(path.join(basepath, 'A4.xlsx'))])

    counts_train_flat_A1 = list(counts_A1.flat)
    train_labels_A1 = pd.DataFrame()
    y_train_A1_list = []
    y_val_A1_list = []
    y_test_A1_list = []
    for f in counts_train_flat_A1:
        frame = pd.read_excel(f, header=None)
        train_labels_A1 = train_labels_A1.append(frame, ignore_index=False)
    all_labels_A1 = np.array(train_labels_A1)

    for j in filelist_train_A1_img:
        arr_idx = np.where(all_labels_A1 == j)
        y_train_A1_list.append(all_labels_A1[arr_idx[0], :])
    y_train_A1_labels = np.concatenate(y_train_A1_list, axis=0)

    for j in filelist_val_A1_img:
        arr_idx = np.where(all_labels_A1 == j)
        y_val_A1_list.append(all_labels_A1[arr_idx[0], :])
    y_val_A1_labels = np.concatenate(y_val_A1_list, axis=0)

    for j in filelist_test_A1_img:
        arr_idx = np.where(all_labels_A1 == j)
        y_test_A1_list.append(all_labels_A1[arr_idx[0], :])
    y_test_A1_labels = np.concatenate(y_test_A1_list, axis=0)

    counts_train_flat_A2 = list(counts_A2.flat)
    train_labels_A2 = pd.DataFrame()
    y_train_A2_list = []
    y_val_A2_list = []
    y_test_A2_list = []
    for f in counts_train_flat_A2:
        frame = pd.read_excel(f, header=None)
        train_labels_A2 = train_labels_A2.append(frame, ignore_index=False)
    all_labels_A2 = np.array(train_labels_A2)

    for j in filelist_train_A2_img:
        arr_idx = np.where(all_labels_A2 == j)
        y_train_A2_list.append(all_labels_A2[arr_idx[0], :])
    y_train_A2_labels = np.concatenate(y_train_A2_list, axis=0)

    for j in filelist_val_A2_img:
        arr_idx = np.where(all_labels_A2 == j)
        y_val_A2_list.append(all_labels_A2[arr_idx[0], :])
    y_val_A2_labels = np.concatenate(y_val_A2_list, axis=0)

    for j in filelist_test_A2_img:
        arr_idx = np.where(all_labels_A2 == j)
        y_test_A2_list.append(all_labels_A2[arr_idx[0], :])
    y_test_A2_labels = np.concatenate(y_test_A2_list, axis=0)

    counts_train_flat_A3 = list(counts_A3.flat)
    train_labels_A3 = pd.DataFrame()
    y_train_A3_list = []
    y_val_A3_list = []
    y_test_A3_list = []
    for f in counts_train_flat_A3:
        frame = pd.read_excel(f, header=None)
        train_labels_A3 = train_labels_A3.append(frame, ignore_index=False)
    all_labels_A3 = np.array(train_labels_A3)

    for j in filelist_train_A3_img:
        arr_idx = np.where(all_labels_A3 == j)
        y_train_A3_list.append(all_labels_A3[arr_idx[0], :])
    y_train_A3_labels = np.concatenate(y_train_A3_list, axis=0)

    for j in filelist_val_A3_img:
        arr_idx = np.where(all_labels_A3 == j)
        y_val_A3_list.append(all_labels_A3[arr_idx[0], :])
    y_val_A3_labels = np.concatenate(y_val_A3_list, axis=0)

    for j in filelist_test_A3_img:
        arr_idx = np.where(all_labels_A3 == j)
        y_test_A3_list.append(all_labels_A3[arr_idx[0], :])
    y_test_A3_labels = np.concatenate(y_test_A3_list, axis=0)

    counts_train_flat_A4 = list(counts_A4.flat)
    train_labels_A4 = pd.DataFrame()
    y_train_A4_list = []
    y_val_A4_list = []
    y_test_A4_list = []
    for f in counts_train_flat_A4:
        frame = pd.read_excel(f, header=None)
        train_labels_A4 = train_labels_A4.append(frame, ignore_index=False)
    all_labels_A4 = np.array(train_labels_A4)

    for j in filelist_train_A4_img:
        arr_idx = np.where(all_labels_A4 == j)
        y_train_A4_list.append(all_labels_A4[arr_idx[0], :])
    y_train_A4_labels = np.concatenate(y_train_A4_list, axis=0)

    for j in filelist_val_A4_img:
        arr_idx = np.where(all_labels_A4 == j)
        y_val_A4_list.append(all_labels_A4[arr_idx[0], :])
    y_val_A4_labels = np.concatenate(y_val_A4_list, axis=0)

    for j in filelist_test_A4_img:
        arr_idx = np.where(all_labels_A4 == j)
        y_test_A4_list.append(all_labels_A4[arr_idx[0], :])
    y_test_A4_labels = np.concatenate(y_test_A4_list, axis=0)

    y_train_all_labels = np.concatenate((y_train_A1_labels, y_train_A2_labels, y_train_A3_labels, y_train_A4_labels),
                                        axis=0)
    y_val_all_labels = np.concatenate((y_val_A1_labels, y_val_A2_labels, y_val_A3_labels, y_val_A4_labels), axis=0)
    y_test_all_labels = np.concatenate((y_test_A1_labels, y_test_A2_labels, y_test_A3_labels, y_test_A4_labels), axis=0)

    y_train_all = y_train_all_labels[:, 1]
    y_val_all = y_val_all_labels[:, 1]
    y_test_all = y_test_all_labels[:, 1]


    return x_train_all, x_val_all, x_test_all, y_train_all, y_val_all, y_test_all, x_train_set, x_val_set, x_test_set, x_train_img, x_val_img, x_test_img

# def get_data_testing_mask(basepath='CVPPP2017_testing/testing'):
#     ###############################
#     # Getting images (x data)	  #
#     ###############################
#
#
#     imgname_test_A1 = np.array([glob.glob(path.join(basepath,'A1','*fg.png'))])
#     imgname_test_A2 = np.array([glob.glob(path.join(basepath,'A2','*fg.png'))])
#     imgname_test_A3 = np.array([glob.glob(path.join(basepath,'A3','*fg.png'))])
#     imgname_test_A4 = np.array([glob.glob(path.join(basepath,'A4','*fg.png'))])
#     imgname_test_A5 = np.array([glob.glob(path.join(basepath,'A5','*fg.png'))])
#
#
#     filelist_test_A1 = list(np.sort(imgname_test_A1.flat))
#     filelist_test_A2 = list(np.sort(imgname_test_A2.flat))
#     filelist_test_A3 = list(np.sort(imgname_test_A3.flat))
#     filelist_test_A4 = list(np.sort(imgname_test_A4.flat))
#     filelist_test_A5 = list(np.sort(imgname_test_A5.flat))
#
#
#     filelist_test_A1_img = np.array([np.array(get_filename(filelist_test_A1[h])) for h in range(len(filelist_test_A1))])
#     filelist_test_A2_img = np.array([np.array(get_filename(filelist_test_A2[h])) for h in range(len(filelist_test_A2))])
#     filelist_test_A3_img = np.array([np.array(get_filename(filelist_test_A3[h])) for h in range(len(filelist_test_A3))])
#     filelist_test_A4_img = np.array([np.array(get_filename(filelist_test_A4[h])) for h in range(len(filelist_test_A4))])
#     filelist_test_A5_img = np.array([np.array(get_filename(filelist_test_A5[h])) for h in range(len(filelist_test_A5))])
#     filelist_test_A1_set = np.array([np.array(get_last_dir(filelist_test_A1[h])) for h in range(len(filelist_test_A1))])
#     filelist_test_A2_set = np.array([np.array(get_last_dir(filelist_test_A2[h])) for h in range(len(filelist_test_A2))])
#     filelist_test_A3_set = np.array([np.array(get_last_dir(filelist_test_A3[h])) for h in range(len(filelist_test_A3))])
#     filelist_test_A4_set = np.array([np.array(get_last_dir(filelist_test_A4[h])) for h in range(len(filelist_test_A4))])
#     filelist_test_A5_set = np.array([np.array(get_last_dir(filelist_test_A5[h])) for h in range(len(filelist_test_A5))])
#
#     x_test_A1 = np.array([np.array(misc.imread(fname)) for fname in filelist_test_A1])
#     x_test_A2 = np.array([np.array(Image.open(fname)) for fname in filelist_test_A2])
#     x_test_A3 = np.array([np.array(Image.open(fname)) for fname in filelist_test_A3])
#     x_test_A4 = np.array([np.array(Image.open(fname)) for fname in filelist_test_A4])
#     x_test_A5 = [np.array(Image.open(fname)) for fname in filelist_test_A5]
#
#     x_test_A5 = [x[:,:,np.newaxis] if len(x.shape)==2 else x for x in x_test_A5]
#     x_test_A5 = [np.repeat(x,3,2) if x.shape[2] == 1 else x for x in x_test_A5]
#     x_test_A5 = [misc.imresize(x_test_A5[i],[320,320,3]) for i in range(len(x_test_A5))]
#
#     x_test_A2 = x_test_A2[:, :, :, np.newaxis]; x_test_A2 = np.repeat(x_test_A2, 3, 3)
#     x_test_A3 = x_test_A3[:, :, :, np.newaxis];x_test_A3 = np.repeat(x_test_A3, 3, 3)
#     x_test_A4 = x_test_A4[:, :, :, np.newaxis];x_test_A4 = np.repeat(x_test_A4, 3, 3)
#
#     x_test_res_A1 = np.array([misc.imresize(x_test_A1[i],[320,320,3]) for i in range(0,len(x_test_A1))])
#     x_test_res_A2 = np.array([misc.imresize(x_test_A2[i],[320,320,3]) for i in range(0,len(x_test_A2))])
#     x_test_res_A3 = np.array([misc.imresize(x_test_A3[i],[320,320,3]) for i in range(0,len(x_test_A3))])
#     x_test_res_A4 = np.array([misc.imresize(x_test_A4[i],[320,320,3]) for i in range(0,len(x_test_A4))])
#     x_test_res_A5 = np.array(x_test_A5)
#     #x_test_res_A5 = np.array([misc.imresize(x_test_A5[i],[320,320,3]) for i in range(0,len(x_test_A5))])
#
#
#
#     x_test_all = np.concatenate((x_test_res_A1, x_test_res_A2, x_test_res_A3, x_test_res_A4,x_test_res_A5), axis=0)
#     test_sets  = np.concatenate((filelist_test_A1_set,filelist_test_A2_set,filelist_test_A3_set,filelist_test_A4_set,filelist_test_A5_set))
#
#
#
#     return x_test_all,test_sets


def get_data_testing(basepath='CVPPP2017_testing/testing'):
    ###############################
    # Getting images (x data)	  #
    ###############################


    imgname_test_A1 = np.array([glob.glob(path.join(basepath,'A1','*rgb.png'))])
    imgname_test_A2 = np.array([glob.glob(path.join(basepath,'A2','*rgb.png'))])
    imgname_test_A3 = np.array([glob.glob(path.join(basepath,'A3','*rgb.png'))])
    imgname_test_A4 = np.array([glob.glob(path.join(basepath,'A4','*rgb.png'))])
    imgname_test_A5 = np.array([glob.glob(path.join(basepath,'A5','*rgb.png'))])


    filelist_test_A1 = list(np.sort(imgname_test_A1.flat))
    filelist_test_A2 = list(np.sort(imgname_test_A2.flat))
    filelist_test_A3 = list(np.sort(imgname_test_A3.flat))
    filelist_test_A4 = list(np.sort(imgname_test_A4.flat))
    filelist_test_A5 = list(np.sort(imgname_test_A5.flat))


    filelist_test_A1_img = np.array([np.array(get_filename(filelist_test_A1[h])) for h in range(len(filelist_test_A1))])
    filelist_test_A2_img = np.array([np.array(get_filename(filelist_test_A2[h])) for h in range(len(filelist_test_A2))])
    filelist_test_A3_img = np.array([np.array(get_filename(filelist_test_A3[h])) for h in range(len(filelist_test_A3))])
    filelist_test_A4_img = np.array([np.array(get_filename(filelist_test_A4[h])) for h in range(len(filelist_test_A4))])
    filelist_test_A5_img = np.array([np.array(get_filename(filelist_test_A5[h])) for h in range(len(filelist_test_A5))])
    filelist_test_A1_set = np.array([np.array(get_last_dir(filelist_test_A1[h])) for h in range(len(filelist_test_A1))])
    filelist_test_A2_set = np.array([np.array(get_last_dir(filelist_test_A2[h])) for h in range(len(filelist_test_A2))])
    filelist_test_A3_set = np.array([np.array(get_last_dir(filelist_test_A3[h])) for h in range(len(filelist_test_A3))])
    filelist_test_A4_set = np.array([np.array(get_last_dir(filelist_test_A4[h])) for h in range(len(filelist_test_A4))])
    filelist_test_A5_set = np.array([np.array(get_last_dir(filelist_test_A5[h])) for h in range(len(filelist_test_A5))])

    x_test_img = np.concatenate((filelist_test_A1_img, filelist_test_A2_img, filelist_test_A3_img, filelist_test_A4_img,filelist_test_A5_img), axis=0)

    x_test_A1 = np.array([np.array(misc.imread(fname)) for fname in filelist_test_A1])
    x_test_A1 = np.delete(x_test_A1,3,3)
    x_test_A2 = np.array([np.array(Image.open(fname)) for fname in filelist_test_A2])
    x_test_A2 = np.delete(x_test_A2,3,3)
    x_test_A3 = np.array([np.array(Image.open(fname)) for fname in filelist_test_A3])
    #x_test_A3 = np.delete(x_test_A3, 3, 3)
    x_test_A4 = np.array([np.array(Image.open(fname)) for fname in filelist_test_A4])
    #x_test_A4 = np.delete(x_test_A4, 3, 3)
    x_test_A5 = np.array([np.array(Image.open(fname)) for fname in filelist_test_A5])
    #x_test_A5 = np.delete(x_test_A5, 3, 3)

    for i in range(len(x_test_A5)):
        x_A5_img = x_test_A5[i]
        if x_A5_img.shape[2] == 4:
            x_A5_img_del = np.delete(x_A5_img,3,2)
            x_test_A5[i] = x_A5_img_del

    x_test_res_A1 = np.array([misc.imresize(x_test_A1[i],[320,320,3]) for i in range(0,len(x_test_A1))])
    x_test_res_A2 = np.array([misc.imresize(x_test_A2[i],[320,320,3]) for i in range(0,len(x_test_A2))])
    x_test_res_A3 = np.array([misc.imresize(x_test_A3[i],[320,320,3]) for i in range(0,len(x_test_A3))])
    x_test_res_A4 = np.array([misc.imresize(x_test_A4[i],[320,320,3]) for i in range(0,len(x_test_A4))])
    x_test_res_A5 = np.array([misc.imresize(x_test_A5[i],[320,320,3]) for i in range(0,len(x_test_A5))])

    for h in range(0,len(x_test_res_A1)):
        x_img = x_test_res_A1[h]
        x_img_pil = Image.fromarray(x_img)
        x_img_pil = ImageOps.autocontrast(x_img_pil)
        x_img_ar = np.array(x_img_pil)
        x_test_res_A1[h] = x_img_ar

    for h in range(0,len(x_test_res_A2)):
        x_img = x_test_res_A2[h]
        x_img_pil = Image.fromarray(x_img)
        x_img_pil = ImageOps.autocontrast(x_img_pil)
        x_img_ar = np.array(x_img_pil)
        x_test_res_A2[h] = x_img_ar

    for h in range(0,len(x_test_res_A3)):
        x_img = x_test_res_A3[h]
        x_img_pil = Image.fromarray(x_img)
        x_img_pil = ImageOps.autocontrast(x_img_pil)
        x_img_ar = np.array(x_img_pil)
        x_test_res_A3[h] = x_img_ar

    for h in range(0,len(x_test_res_A4)):
        x_img = x_test_res_A4[h]
        x_img_pil = Image.fromarray(x_img)
        x_img_pil = ImageOps.autocontrast(x_img_pil)
        x_img_ar = np.array(x_img_pil)
        x_test_res_A4[h] = x_img_ar

    for h in range(0,len(x_test_res_A5)):
        x_img = x_test_res_A5[h]
        x_img_pil = Image.fromarray(x_img)
        x_img_pil = ImageOps.autocontrast(x_img_pil)
        x_img_ar = np.array(x_img_pil)
        x_test_res_A5[h] = x_img_ar

    ###############################
    # Getting targets (y data)	  #
    ###############################
    counts_A1 = np.array([glob.glob(path.join(basepath, 'A1/A1.xlsx'))])
    counts_A2 = np.array([glob.glob(path.join(basepath, 'A2/A2.xlsx'))])
    counts_A3 = np.array([glob.glob(path.join(basepath, 'A3/A3.xlsx'))])
    counts_A4 = np.array([glob.glob(path.join(basepath, 'A4/A4.xlsx'))])
    counts_A5 = np.array([glob.glob(path.join(basepath, 'A5/A5.xlsx'))])

    counts_train_flat_A1 = list(counts_A1.flat)
    train_labels_A1 = pd.DataFrame()
    y_test_A1_list = []
    for f in counts_train_flat_A1:
        frame = pd.read_excel(f, header=None)
        train_labels_A1 = train_labels_A1.append(frame, ignore_index=False)
    all_labels_A1 = np.array(train_labels_A1)

    for j in filelist_test_A1_img:
        arr_idx = np.where(all_labels_A1 == j)
        y_test_A1_list.append(all_labels_A1[arr_idx[0], :])
    y_test_A1_labels = np.concatenate(y_test_A1_list, axis=0)

    counts_train_flat_A2 = list(counts_A2.flat)
    train_labels_A2 = pd.DataFrame()
    y_test_A2_list = []
    for f in counts_train_flat_A2:
        frame = pd.read_excel(f, header=None)
        train_labels_A2 = train_labels_A2.append(frame, ignore_index=False)
    all_labels_A2 = np.array(train_labels_A2)


    for j in filelist_test_A2_img:
        arr_idx = np.where(all_labels_A2 == j)
        y_test_A2_list.append(all_labels_A2[arr_idx[0], :])
    y_test_A2_labels = np.concatenate(y_test_A2_list, axis=0)

    counts_train_flat_A3 = list(counts_A3.flat)
    train_labels_A3 = pd.DataFrame()
    y_test_A3_list = []
    for f in counts_train_flat_A3:
        frame = pd.read_excel(f, header=None)
        train_labels_A3 = train_labels_A3.append(frame, ignore_index=False)
    all_labels_A3 = np.array(train_labels_A3)


    for j in filelist_test_A3_img:
        arr_idx = np.where(all_labels_A3 == j)
        y_test_A3_list.append(all_labels_A3[arr_idx[0], :])
    y_test_A3_labels = np.concatenate(y_test_A3_list, axis=0)

    counts_train_flat_A4 = list(counts_A4.flat)
    train_labels_A4 = pd.DataFrame()
    y_test_A4_list = []
    for f in counts_train_flat_A4:
        frame = pd.read_excel(f, header=None)
        train_labels_A4 = train_labels_A4.append(frame, ignore_index=False)
    all_labels_A4 = np.array(train_labels_A4)

    for j in filelist_test_A4_img:
        arr_idx = np.where(all_labels_A4 == j)
        y_test_A4_list.append(all_labels_A4[arr_idx[0], :])
    y_test_A4_labels = np.concatenate(y_test_A4_list, axis=0)


    #################
    counts_train_flat_A5 = list(counts_A5.flat)
    train_labels_A5 = pd.DataFrame()
    y_test_A5_list = []
    for f in counts_train_flat_A5:
        frame = pd.read_excel(f, header=None)
        train_labels_A5 = train_labels_A5.append(frame, ignore_index=False)
    all_labels_A5 = np.array(train_labels_A5)

    for j in filelist_test_A5_img:
        arr_idx = np.where(all_labels_A5 == j)
        y_test_A5_list.append(all_labels_A5[arr_idx[0], :])
    y_test_A5_labels = np.concatenate(y_test_A5_list, axis=0)
    #################


    x_test_all = np.concatenate((x_test_res_A1, x_test_res_A2, x_test_res_A3, x_test_res_A4, x_test_res_A5), axis=0)
    y_test_all = np.concatenate((y_test_A1_labels, y_test_A2_labels, y_test_A3_labels, y_test_A4_labels,y_test_A5_labels),axis=0)
    test_sets  = np.concatenate((filelist_test_A1_set,filelist_test_A2_set,filelist_test_A3_set,filelist_test_A4_set,filelist_test_A5_set))


    y_test_all = y_test_all[:, 1]


    return x_test_all,y_test_all,test_sets,x_test_img













if __name__ == "__main__":
    D = CVPPPDataset("/media/vgiuffrida/PHDDATA/Data/Plant Phenotyping/CVPPP_2017/CVPPP2017_LCC_training/training/",folders=[1, 2, 4])
    D.load(new_size=(320,320))
    data = D.getData(['rgb', 'count'])
    np.savez('cvppp17_data.npz', data=[data])

    D = CVPPPDataset("/media/vgiuffrida/PHDDATA/Data/Plant Phenotyping/CVPPP_2017/CVPPP2017_testing/testing/",folders=[1,2,4])
    D.load(new_size=(320, 320))
    data = D.getData(['rgb','count'])
    np.savez('cvppp17_data_testing.npz',data=[data])


