#!/bin/bash

export CUDA_VISIBLE_DEVICES=3

TARGET_PATH="../RDS/CVPPP2017/CVPPP2017_LCC_training/TrainingSplits"
SOURCE_PATH="../RDS/mm"

python exp_intraspecies_cvppp.py --source-path $SOURCE_PATH --target-path $TARGET_PATH --split 0 -m trained_models/pretrained_mm/mm_pretrained_split_multi_0.h5 -o trained_models/intra_species_mm2cvppp/split0 --generator-loops 2 --loss mse --multi
python exp_intraspecies_cvppp.py --source-path $SOURCE_PATH --target-path $TARGET_PATH --split 1 -m trained_models/pretrained_mm/mm_pretrained_split_multi_0.h5 -o trained_models/intra_species_mm2cvppp/split1 --generator-loops 2 --loss mse --multi
python exp_intraspecies_cvppp.py --source-path $SOURCE_PATH --target-path $TARGET_PATH --split 2 -m trained_models/pretrained_mm/mm_pretrained_split_multi_0.h5 -o trained_models/intra_species_mm2cvppp/split2 --generator-loops 2 --loss mse --multi
python exp_intraspecies_cvppp.py --source-path $SOURCE_PATH --target-path $TARGET_PATH --split 3 -m trained_models/pretrained_mm/mm_pretrained_split_multi_0.h5 -o trained_models/intra_species_mm2cvppp/split3 --generator-loops 2 --loss mse --multi

